<?php

declare(strict_types=1);

namespace TechTic\AnanthPractical\Block;

use TechTic\AnanthPractical\Helper\Data;

class OfferOfDay extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \TechTic\AnanthPractical\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $product = $this->helper->getProduct();
        $link = '';
        if ($product) {
            $link = '<li class="level0 nav-6 category-item last level-top ui-menu-item" role="presentation">';
            $link .= '<a href="' . $product->getProductUrl() . '" class="level-top ui-corner-all" id="ui-id-7" tabindex="-1" role="menuitem" style="font-weight: 600;color:#ff5501"><span>Offer Of the Day</span></a></li>';
        }

        return $link;
    }
}
