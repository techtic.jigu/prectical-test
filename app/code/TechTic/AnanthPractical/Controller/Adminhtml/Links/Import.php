<?php
namespace TechTic\AnanthPractical\Controller\Adminhtml\Links;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\Controller\ResultFactory;

class Import extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    const ADMIN_RESOURCE = 'TechTic_AnanthPractical::import';

    /**
     * Import and export Page
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        $resultPage->setActiveMenu('TechTic_AnanthPractical::import_links');
        $resultPage->addContent(
            $resultPage->getLayout()->createBlock(\TechTic\AnanthPractical\Block\Adminhtml\Import::class)
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Upsell, Crossell, Related'));
        $resultPage->getConfig()->getTitle()->prepend(__('Import Upsell, Crossell, Related to the Products'));
        return $resultPage;
    }
}
