<?php

namespace TechTic\AnanthPractical\Controller\Adminhtml\Links;

use Magento\Framework\Controller\ResultFactory;

class ImportPost extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'TechTic_AnanthPractical::import_links';

    /**
     * import action from import/export tax
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $importFile = $this->getRequest()->getFiles('import_links_file');
        if ($this->getRequest()->isPost() && isset($importFile['tmp_name'])) {
            try {
                /** @var \TechTic\AnanthPractical\Model\Product\CsvImportHandler $importHandler */
                $importHandler = $this->_objectManager->create(
                    \TechTic\AnanthPractical\Model\Product\CsvImportHandler::class
                );
                $basePath = $importHandler->importFromCsvFile($importFile);

                $this->messageManager->addSuccess(__('The Upsell, Crossell, Related has been imported.'));
                $this->messageManager->addSuccess(__('You can download files from: ' . $basePath));
            } catch (\Magento\Framework\Exception\LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addError(__($e->getMessage()));
                $this->messageManager->addError(__('Invalid file upload attempt'));
            }
        } else {
            $this->messageManager->addError(__('Invalid file upload attempt'));
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $resultRedirect->setUrl($this->_redirect->getRedirectUrl());
        return $resultRedirect;
    }

    /**
     * Is the user allowed to view the page.
    *
    * @return bool
    */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(static::ADMIN_RESOURCE);
    }
}
