<?php

declare(strict_types=1);

namespace TechTic\AnanthPractical\Helper;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Catalog\Model\ProductRepository;

/**
 * @package TechTic\AnanthPractical\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @param Context $context
     */
    public function __construct(
        Context $context,
        ScopeConfigInterface $scopeConfig,
        ProductRepository $productRepository
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->productRepository = $productRepository;
    }

    /**
     * @return ProductInterface|false
     */
    public function getProduct()
    {
        $sku = $this->scopeConfig->getValue('techtic/general/sku');
        $sku = !empty($sku) ? trim($sku) : '';

        if (!empty($sku)) {
            $sku = explode(',', $sku)[0];
            try {
                $product = $this->productRepository->get($sku);

                return $product;
            } catch (\Exception $e) {
                echo $e->getMessage();exit;
            }
        }
        return false;
    }
}
