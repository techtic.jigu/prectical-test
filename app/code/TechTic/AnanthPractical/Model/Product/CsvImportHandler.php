<?php

namespace TechTic\AnanthPractical\Model\Product;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\File\Csv;
use Magento\Framework\Filesystem\Io\File;

class CsvImportHandler
{
    /**
     * @var ProductLinkInterfaceFactory
     */
    protected $productLinkFactory;

    /**
     * @var Csv
     */
    protected $csvProcessor;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var File
     */
    protected $fs;

    protected $directoryList;

    public function __construct(
        \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $productLinkFactory,
        \Magento\Framework\File\Csv $csvProcessor,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Filesystem\Io\File $fs,
        \Magento\Framework\Filesystem\DirectoryList $directoryList
    ) {
        $this->productLinkFactory = $productLinkFactory;
        $this->csvProcessor = $csvProcessor;
        $this->productRepository = $productRepository;
        $this->fs = $fs;
        $this->directoryList = $directoryList;
    }

    /**
     * Retrieve a list of fields required for CSV file (order is important!)
     *
     * @return array
     */
    public function getRequiredCsvFields()
    {
        // indexes are specified for clarity, they are used during import
        return [
            0 => __('sku'),
            1 => __('upsell_skus'),
            2 => __('crossell_skus'),
            3 => __('related_skus')
        ];
    }

    /**
     * Import Tax Rates from CSV file
     *
     * @param array $file file info retrieved from $_FILES array
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function importFromCsvFile($file)
    {
        if (!isset($file['tmp_name'])) {
            throw new \Magento\Framework\Exception\LocalizedException(__('Invalid file upload attempt.'));
        }
        $rawData = $this->csvProcessor->getData($file['tmp_name']);

        $basePath = $this->directoryList->getPath('var');

        // csv file to 'var/importexport'
        $this->fs->mkdir($basePath . '/importexport/');
        $this->fs->cp($file['tmp_name'], $basePath . '/importexport/' . $file['name']);

        // first row of file represents headers
        $fileFields = $rawData[0];

        foreach ($rawData as $rowIndex => $dataRow) {
            // skip headers
            if ($rowIndex == 0) {
                continue;
            }
        }

        $this->importLinks($rawData, $fileFields);

        return $basePath . '/importexport/';
    }

    protected function importLinks($linkData, $fileFields)
    {
        foreach ($linkData as $rowIndex => $row) {
            $linkData = array_combine($fileFields, $row);
            // skip headers
            if ($rowIndex == 0) {
                continue;
            }
            $product = null;
            try {
                $product = $this->productRepository->get($linkData['sku'], true);
            } catch (\Throwable $th) {
                throw new \Magento\Framework\Exception\LocalizedException(__('Error on product not found on line number ' . ($rowIndex)));
            }
            $existing = $product->getProductLinks();
            $linkDataAll = $existing ?? [];
            $sku = $linkData['sku'];

            foreach ($linkData as $key => $value) {
                if (empty(trim($value))) {
                    break;
                }
                $value = explode(',', $value);
                switch ($key) {
                    case 'sku';
                        break;
                    case 'upsell_skus':
                        foreach ($value as $key => $v) {
                            $linkData = $this->productLinkFactory->create()
                            ->setSku($sku)
                            ->setLinkedProductSku($v)
                            ->setLinkType("upsell")
                            ->setPosition($rowIndex + 1);
                            $linkDataAll[] = $linkData;
                        }
                        break;

                    case 'crossell_skus':
                        foreach ($value as $key => $v) {
                            $linkData = $this->productLinkFactory->create()
                            ->setSku($sku)
                            ->setLinkedProductSku($v)
                            ->setLinkType("crosssell")
                            ->setPosition($rowIndex + 1);
                            $linkDataAll[] = $linkData;
                        }
                        break;

                    case 'related_skus':
                        foreach ($value as $key => $v) {
                            $linkData = $this->productLinkFactory->create()
                            ->setSku($sku)
                            ->setLinkedProductSku($v)
                            ->setLinkType("related")
                            ->setPosition($rowIndex + 1);
                            $linkDataAll[] = $linkData;
                        }
                        break;
                }
            }
            $product->setProductLinks($linkDataAll);

            $this->productRepository->save($product);

            $linkDataAll = [];

        }
    }
}
