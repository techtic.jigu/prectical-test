## Test task1 - admin.  - DONE

Platform: Magento 2x

Implement the functionality with next requirements:

1. Display in the header and footer block "Offer of the day"

2. Should contain SKU of the product specified in the configuration.

## Test task2 - admin. - DONE

Platform: Magento 2x

Create a module to connect upsell, crossell, related products based on information from a CSV file.

Requirements

1. Create a form in the administrative part to upload and import the file

3. Downloaded files must remain on disk in /var/importexport

2. Access to the form must be restricted (acl)

3. Columns: sku, upsell_skus, crossell_skus, related_skus

## Test task - front.  - DONE

Platform: Magento 2x

Develop a module that:

1. Create a text type product attribute to store html data

2. Will display the value of this attribute on the product page in a separate tab or in 'container2'

3. Where it will be displayed and whether it will be displayed - make a choice in the configuration.

Let me know if you have any questions or difficulties. I would be happy to help you.